variable "aws_region" {
  description = "region used for aws"
  type        = string
  default     = "us-west-2"
}