output "root_module_says" {
    value = "hello from root module"
}

# output "ec2_instand_id" {
#     value = "${module.ec2-resouces.instance_id}"
# }

# output "ec2_instance_public_ip" {
#     value = "${module.ec2-resouces.instance_public_ip}"
# }

output "s3_bucket_id_output" {
    value = "${module.s3-resouces.s3_bucket_id}"
}