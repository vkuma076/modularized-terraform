terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = var.aws_region
}

module "ec2-resouces" {
  source = "./modules/ec2"
}

module "s3-resouces" {
  source = "./modules/s3"
}

