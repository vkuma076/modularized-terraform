variable "iam_role_name" {
    description = "var name for iam role"
    type        = string
    default     = "vin_iam_role"
}