variable "bucket_name" {
    description = "var name for s3 bucket"
    type        = string
    default     = "vin-tf-test-bucket"
}