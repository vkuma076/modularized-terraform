resource "aws_s3_bucket" "vin_s3_bucket" {
  bucket = var.bucket_name

  tags = {
    Name        = "vin-test-tf-bucket-tag"
    Environment = "Dev"
  }
}

resource "aws_s3_bucket_acl" "example" {
  bucket = aws_s3_bucket.vin_s3_bucket.id
  acl    = "private"
}

resource "aws_s3_object" "object" {
  bucket = aws_s3_bucket.vin_s3_bucket.id
  key    = "zoo-tdd-demo-0.0.1-SNAPSHOT"
  source = "./zoo-tdd-demo-0.0.1-SNAPSHOT.jar"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  # etag = filemd5("./zoo-tdd-demo-0.0.1-SNAPSHOT.jar")
}