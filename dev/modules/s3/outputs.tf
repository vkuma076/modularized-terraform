output "s3_bucket_id" {
  description = "s3 bucket id"
  value       = aws_s3_bucket.vin_s3_bucket.id
}

output "s3_bucket_name" {
  description = "s3 bucket id"
  value       = var.bucket_name
}