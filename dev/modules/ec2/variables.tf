variable "ec2_name" {
  description = "var name for ec2 instance"
  type        = string
  default     = "vin-ec2-instance-var"
}