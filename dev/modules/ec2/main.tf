module "iam_resouces" {
  source = "../iam"
}
resource "aws_instance" "vin_ec2_intance" {
  ami           = "ami-08d70e59c07c61a3a"
  instance_type = "t2.micro"
  vpc_security_group_ids = [aws_security_group.allow_tcp.id]
  iam_instance_profile = "${module.iam_resouces.ec2_profile_name}"
  user_data = <<EOF
    #!/bin/bash
    sudo yum update -y
    sudo yum install -y java-11-amazon-corretto-headless
    aws s3api get-object --bucket "vin-tf-test-bucket" --key "zoo-tdd-demo-0.0.1-SNAPSHOT.jar" "zoo-tdd-demo-0.0.1-SNAPSHOT.jar"
    java -jar "zoo-tdd-demo-0.0.1-SNAPSHOT.jar"
  EOF

  tags = {
    Name = var.ec2_name
  }
}

resource "aws_default_vpc" "default_vpc" {
  tags = {
    Name = "Default VPC"
  }
}

resource "aws_security_group" "allow_tcp" {
  name        = "tomcat-default-vin"
  description = "Allow TLS inbound traffic"
  vpc_id      = aws_default_vpc.default_vpc.id

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["162.211.34.152/32"]
  }

  ingress {
    description      = "CUSTOM TCP"
    from_port        = 8080
    to_port          = 8080
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_tcp"
  }
}