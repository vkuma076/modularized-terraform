output "instance_id" {
  description = "ID of the EC2 instance"
  value       = aws_instance.vin_ec2_intance.id
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.vin_ec2_intance.public_ip
}